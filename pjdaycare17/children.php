<?php

if (false) {
    $app = new \Slim\Slim();
}
// CRUD
// show list attendance child
//List
// STATE 1: first show
$app->get('/admin/child/:action(/:id)', function($action, $id = 0) use ($app) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        //$app->redirect('/forbidden?param='. rand());
        //$app->response()->setStatus(403);
        $app->render('forbidden.html.twig');
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    if ($action == 'add') {
        $role = 'parent';
        $userParents =  DB::query("SELECT * FROM users WHERE role=%s", $role);
        $app->render('admin/child_addedit.html.twig', array('userParents'=> $userParents));
    } else { // edit
        $child = DB::queryFirstRow("SELECT ch.*, us.first_name as name, "
                . " us.last_name as secondname, us.role, us.phone_number, "
                . " us.image as parentimage, us.user_id as p_id"
                . " FROM users as us "
                . " INNER JOIN children as ch ON us.user_id = ch.user_id "
                . " WHERE child_id=%i", $id);
        
        $role = 'parent'; //filter parents
        $userParents =  DB::query("SELECT * FROM users WHERE role=%s", $role);
       
        //display parents authorization for pickup
        $parentId = $child[user_id];
        $familyPickUp = DB::query("SELECT pa.*, us.first_name as name, "
                . " us.last_name as secondname, us.role, us.phone_number, "
                . " us.image as parentimage, us.user_id as p_id"
                . " FROM users as us "
                . " INNER JOIN parents as pa ON us.user_id = pa.user_id "
                . " WHERE us.user_id=%i", $parentId);
        
        echo print_r($parents) . " -parents- " . $parentId;
        
        if (!$child) {
          $app->notFound();
          return;
          } 
        print_r($user);
        $app->render('admin/child_addedit.html.twig', array('v' => $child, 'userParents'=> $userParents , 'familyPickUp' => $familyPickUp));
        //echo print_r($child);
        //echo print_r(userParents);
    }
})->conditions(array('action' => '(add|edit)'));

$app->post('/admin/child/:action(/:id)', function($action, $id = 0) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden?param=' . rand());
        echo "error 02";
        return;
    }
    if (($action == 'add' && $id != 0) || ($action == 'edit' && $id == 0)) {
        $app->notFound(); // 404 page
        return;
    }
    //
    $user_id = $app->request()->post('user_id');
    $first_name = $app->request()->post('first_name');
    $last_name = $app->request()->post('last_name');
    $gender = $app->request()->post('gender');
    $birthdate = $app->request()->post('birthdate');
    $blood_type = $app->request()->post('blood_type');
    $alergies = $app->request()->post('alergies');
    $image = $app->request()->post('image');
    $groups = $app->request()->post('groups');
    //
    $errorList = array();
    // FIXME: sanitize html tags in name and description
    if (($user_id) == "") {
        array_push($errorList, "please add parent id");
        $user_id = "";
    }
    if (strlen($first_name) < 2 || strlen($first_name) > 60) {
        array_push($errorList, "First Name must be 2-60 characters long");
        $first_name = "";
    }
    if (strlen($last_name) < 2 || strlen($last_name) > 60) {
        array_push($errorList, "Last Name must be 2-60 characters long");
        $last_name = "";
    }
    if (strlen($gender) < 2 || strlen($gender) > 60) {
        array_push($errorList, "Gender must be male or female");
        $gender = "";
    }

    if (strlen($blood_type) < 2 || strlen($blood_type) > 20) {
        array_push($errorList, "blood type must be 2-20 characters long");
        $blood_type = "";
    }
    if (strlen($alergies) < 2 || strlen($alergies) > 300) {
        array_push($errorList, "alergies must be 2-300 characters long");
        $alergies = "";
    }
    
    if (strlen($groups) < 2 || strlen($groups) > 50) {
        array_push($errorList, "Status must be 2-50 characters long");
        $groups = "";
    }

    // Check if we are changing the image file
    if (isset($_FILES['childImage']) && $_FILES['childImage'] != null) {
        $childImage = $_FILES['childImage'];
        // echo "<pre>111\n"; print_r($childImage); //exit;
        if ($childImage['error'] != 0) {
            array_push($errorList, "File submission failed, make sure you've selected an image (1)");
        } else {
            $data = getimagesize($childImage['tmp_name']);
            if ($data == FALSE) {
                array_push($errorList, "File submission failed, make sure you've selected an image (2)");
            } else {
                if (!in_array($data['mime'], array('image/jpeg', 'image/gif', 'image/png'))) {
                    array_push($errorList, "File submission failed, make sure you've selected an image (3)");
                } else {
                    // FIXME: sanitize file name, otherwise a security hole, maybe
                    $childImage['name'] = strtolower($childImage['name']);
                    if (!preg_match('/.\.(jpg|jpeg|png|gif)$/', $childImage['name'])) {
                        array_push($errorList, "File submission failed, make sure you've selected an image (4)");
                    }
                    $info = pathinfo($childImage['name']);
                    $childImage['name'] = preg_replace('[^a-zA-Z0-9_\.-]', '_', $childImage['name']);
                    if (file_exists('images/' . $childImage['name'])) {
                        // array_push($errorList, "File submission failed, refusing to override existing file (5)");
                        $num = 1;

                        while (file_exists('images/' . $info['filename'] . "_$num." . $info['extension'])) {
                            $num++;
                        }
                        $childImage['name'] = $info['filename'] . "_$num." . $info['extension'];
                    }
                    // RANDOM NAME INSTEAD OF SANITIZATION
                    // $childImage['name'] = RandomString(25) . "." . $info['extension'];
                    // all good, nothing to do for now
                }
            }
        }
    }
    //
    if ($errorList) { // STATE 2: failed submission
        $app->render('admin/child_addedit.html.twig', array(
            'errorList' => $errorList,
            'v' => array('child_id' => $id,
                'user_id' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthdate' => $birthdate,
                'blood_type' => $blood_type,
                'alergies' => $alergies,
                'childImage' => $imagePath,
                'groups' => $groups
        )));
    } else { // STATE 3: successful submission
        $imagePath = 'uploads/' . $childImage['name'];
        // DANGERS: // images/../slimshop17.php
        // 1. what if name begins with .. and escapes to an upper directory?
        // 2. what if the file extension is dangerous, e.g. php
        // 3. file overriding
        // $log->debug("a $imagePath " . $childImage['tmp_name']);
        if (!move_uploaded_file($childImage['tmp_name'], $imagePath)) {
            $log->err("Error moving uploaded file: " . print_r($childImage, true));
            $app->redirect('/internalerror');
            return;
        }
        if ($action == 'add') {
            DB::insert('children', array(
                'user_id' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthdate' => $birthdate,
                'blood_type' => $blood_type,
                'alergies' => $alergies,
                'image' => $imagePath,
                'groups' => $groups
            ));
            $childId = DB::insertId();
            $log->debug("child registed with id=" . $childId);
            $app->render('admin/child_success.html.twig');
        } else {
            // remove the old file
            $oldImagePath = DB::queryFirstField("SELECT image FROM children WHERE child_id=%i", $id);
            if ($oldImagePath != "" && file_exists($oldImagePath)) {
                unlink($oldImagePath);
            }
            DB::update('children', array(
                'user_id' => $user_id,
                'first_name' => $first_name,
                'last_name' => $last_name,
                'gender' => $gender,
                'birthdate' => $birthdate,
                'blood_type' => $blood_type,
                'alergies' => $alergies,
                //'image' => $image,
                'groups' => $groups,
                //'childimage' => $childImage,
                'image' => $imagePath), 'child_id=%i', $id);
            $app->render('admin/child_success.html.twig', array('savedId' => $id));
        }
    }
})->conditions(array('action' => '(add|edit)'));

$app->get('/admin/child/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    $item = DB::queryFirstRow("SELECT * FROM children WHERE child_id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/child_delete.html.twig', array('item' => $item));
});
$app->post('/admin/child/delete/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    if ($app->request()->post('confirmed') == 'true') {
        DB::delete("children", "child_id=%i", $id);
        $app->render('admin/child_delete_success.html.twig');
    } else {
        $app->redirect('/internalerror');
        return;
    }
});

$app->get('/admin/child/list', function() use ($app, $log) {

    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    else {
        $childList = DB::query("SELECT * FROM children");
        $app->render('admin/child_list.html.twig', array('childList' => $childList));
    }
});

$app->get('/ajax/child/list(/:groups)', function($groups ="") use ($app, $log) {
   
    //$authors =  DB::query("SELECT * FROM users ");
    if ($groups) {
        $list = DB::query("SELECT  * from children WHERE groups=%s", $groups);
    } else{
       $list = DB::query("SELECT * FROM children");
    }
   //echo "List====>". print_r($list);
   echo $app->render('admin/table_child_list.html.twig', array('list' => $list));
});

//check-in
$app->get('/admin/child/attendance/add/:id', function($id) use ($app, $log) {
    if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    $item = DB::queryFirstRow("SELECT * FROM children WHERE child_id=%i", $id);
    if (!$item) {
        $app->notFound();
        return;
    }
    $app->render('admin/child_attendance_add.html.twig', array('item' => $item));
});
$app->post('/admin/child/attendance/add/:id', function($id) use ($app, $log) {
    
       if (!isset($_SESSION['user']) || $_SESSION['user']['role'] != 'admin') {
        $app->redirect('/forbidden');
        return;
    }//
    if ($app->request()->post('confirmed') == 'true') {
        $startdate = new DateTime();
         DB::insert('attendance', array(
                'child_id' => $id,
                'attendance_started' => $startdate,
            ));
            $childId = DB::insertId();
            $log->debug("child attendance with id=" . $childId);
            
            $datenow = date("Y/m/d");
            $item = DB::query("SELECT child.*, atten.* FROM children AS child INNER JOIN attendance AS atten ON child.child_id = atten.child_id"
            . " WHERE atten.attendance_started >=%s ", $datenow);
           // echo print_r($item);
        
       $app->render('admin/child_attendance_list.html.twig', array('list'=>$item));
       $app->redirect('/admin/child/attendance/list'); 
       
    } else {
        $app->redirect('/internalerror');
        return;
    }
});


$app->get('/ajax/child/:action(/:id)', function($id ="") use ($app, $log) {
    $role = 'parent';
    $usersParents =  DB::query("SELECT * FROM users WHERE role=%s", $role);
    if ($id ) {
        $list = DB::query("SELECT  users.user_id as id ,users.first_name as userName, users.last_name as userLastName ,users.image FROM users, children WHERE users.id=children.user_Id and users.id =%s and role=%s" , $id, $role);
    } else {
       $list = DB::query("SELECT  users.user_id as id ,users.first_name as userName, users.last_name as userLastName ,users.image FROM users, children WHERE users.id=children.user_Id and role=%s", $role);
    }
   echo $app->render('admin/child_addedit.html.twig', array('list' => $list,'parents'=>$usersParents));
});
 


