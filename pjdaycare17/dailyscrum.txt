<< current date goes here >>
1. What I have done / not done since last Scrum?
2. What I plan to do from this scrum to next? (next 24 hours)
3. Where do I need assistance? What do I need to figure out?
2019-06-21 Jonathan
1. Done since last scrum:
- created description 
- created mockup
- created Database
2. To do until next Scrum:
- setup the initial project
- create the custom dialog for user login
- setup the database
3. Need assistance / figure things out
- testing user validation
2019-06-22 Jonathan
1. Done since last scrum:
- created login form
- created staff form
2. To do until next Scrum:
- create the custom dialog for parents login
- create the custom dialog for staff login
3. Need assistance / figure things out
- setup the initial project
2019-06-23 Jonathan
1. Done since last scrum:
- created child form
- created parents form
2. To do until next Scrum:
- setup the initial project
- create the custom dialog for parents login
- create the custom dialog for child login
3. Need assistance / figure things out
- setup the initial project
2019-06-24 Jonathan
1. Done since last scrum:
- created bill form
- created parents form
2. To do until next Scrum:
- create calendar
- create billing list
- create for pick up list
3. Need assistance / figure things out
- setup the initial project
2019-06-25 Jonathan
1. Done since last scrum:
- setup the initial project
- created list for pick up
- created calendar
2. To do until next Scrum:
- create child form
3. Need assistance / figure things out
- testing profile parents.
2019-06-26
1. Done since last scrum:
- created register with Parents and staff
2. To do until next Scrum:
- create chil form 
3. Need assistance / figure things out
- testing profile parents (problems with add/eddit).
- testing profile child (problems with add/eddit).
- problems when add staff, I need first register user (problem foreign key)
- problems when user is admin, doesn't work.
2019-06-27
1. Done since last scrum:
- created register list with pictures
- created register crud
2. To do until next Scrum:
- create child form
3. Need assistance / figure things out
- testing register with picture.
2019-06-28
1. Done since last scrum:
- created child form
- created list attendance
2. To do until next Scrum:
- check attendance input hours
3. Need assistance / figure things out
- testing input child and change pictures and group
- testing input hours.
2019-06-29
1. Done since last scrum:
- created list attendance add more fields
2. To do until next Scrum:
- check attendance input hours
- check attendance output hours
- create a new table for attendance list out. 
3. Need assistance / figure things out
- testing list attendance in/out
- testing output hours.
2019-06-30
1. Done since last scrum:
-  created html childattendance delete
2. To do until next Scrum:
- check attendance delete
3. Need assistance / figure things out
- testing list attendance in/out
2019-07-01
1. Done since last scrum:
- filter for parent it's done
- attendance delete it's done
2. To do until next Scrum:
- check list of child for pickup
3. Need assistance / figure things out
- testing list filter parent in child form.
2019-07-02
1. Done since last scrum:
- Filter for group attendance is done
- powerpoint presentation done
2. To do until next Scrum:
- check list of child for pickup - attendance-ended filter
3. Need assistance / figure things out
- testing list filter parent in child form.
2019-07-03
1. Done since last scrum:
- Query for list of child for pick up is done
- Query for list of parent is done
2. To do until next Scrum:

3. Need assistance / figure things out
- testing list child for pick up.