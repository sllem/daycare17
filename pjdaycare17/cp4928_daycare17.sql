-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 03, 2019 at 08:19 PM
-- Server version: 10.0.38-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cp4928_daycare17`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `attendance_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `attendance_started` datetime NOT NULL,
  `attendance_ended` datetime DEFAULT NULL,
  `attendance_comments` varchar(300) DEFAULT NULL,
  `staff_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`attendance_id`, `child_id`, `attendance_started`, `attendance_ended`, `attendance_comments`, `staff_id`, `parent_id`) VALUES
(1, 5, '2019-06-27 08:45:00', '2019-06-27 16:30:00', '-', 6, 10),
(2, 5, '2019-06-28 09:30:00', '2019-07-01 05:37:58', '-', 6, 10),
(3, 5, '2019-06-29 04:42:07', '2019-07-01 05:39:44', NULL, 0, 0),
(4, 10, '2019-06-29 04:50:18', '2019-07-01 05:45:51', NULL, 0, 0),
(5, 10, '2019-06-29 04:50:51', '2019-07-01 05:34:08', NULL, 0, 0),
(6, 8, '2019-06-29 04:57:09', '2019-07-01 05:43:23', NULL, 0, 0),
(8, 1, '2019-06-30 18:50:47', '2019-07-01 05:25:37', NULL, 0, 0),
(9, 5, '2019-06-30 18:52:43', '2019-07-01 05:34:44', NULL, 0, 0),
(10, 8, '2019-07-01 02:31:46', '2019-07-01 05:32:36', NULL, 0, 0),
(11, 8, '2019-07-01 02:31:49', '2019-07-01 05:44:28', NULL, 0, 0),
(13, 9, '2019-07-01 05:12:51', '2019-07-01 05:45:35', NULL, 0, 0),
(14, 10, '2019-07-02 17:47:40', '2019-07-02 15:53:02', NULL, 0, 0),
(15, 1, '2019-07-02 17:55:07', '2019-07-02 18:00:32', NULL, 0, 0),
(19, 7, '2019-07-02 18:05:42', '2019-07-02 19:17:13', NULL, 0, 0),
(29, 8, '2019-07-02 19:15:33', '2019-07-02 19:16:42', NULL, 0, 0),
(31, 9, '2019-07-02 16:30:10', NULL, NULL, 0, 0),
(42, 5, '2019-07-02 23:44:49', '2019-07-03 00:00:25', NULL, 0, 0),
(43, 1, '2019-07-03 00:01:02', '2019-07-03 00:02:39', NULL, 0, 0),
(44, 7, '2019-07-03 00:01:38', '2019-07-03 00:05:13', NULL, 0, 0),
(45, 10, '2019-07-03 00:17:52', '2019-07-03 11:57:27', NULL, 0, 0),
(47, 8, '2019-07-03 18:36:41', '2019-07-03 18:59:44', NULL, 0, 0),
(48, 5, '2019-07-03 19:00:13', NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `color` varchar(7) DEFAULT NULL,
  `start` datetime NOT NULL,
  `end` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`id`, `title`, `color`, `start`, `end`) VALUES
(1, 'All Day Event', '#40E0D0', '2016-01-01 00:00:00', '0000-00-00 00:00:00'),
(2, 'Long Event', '#FF0000', '2016-01-07 00:00:00', '2016-01-10 00:00:00'),
(3, 'Repeating Event', '#0071c5', '2016-01-09 16:00:00', '0000-00-00 00:00:00'),
(4, 'Conference', '#40E0D0', '2016-01-11 00:00:00', '2016-01-13 00:00:00'),
(5, 'Meeting', '#000', '2016-01-12 10:30:00', '2016-01-12 12:30:00'),
(6, 'Lunch', '#0071c5', '2016-01-12 12:00:00', '0000-00-00 00:00:00'),
(7, 'Happy Hour', '#0071c5', '2016-01-12 17:30:00', '0000-00-00 00:00:00'),
(8, 'Dinner', '#0071c5', '2016-01-12 20:00:00', '0000-00-00 00:00:00'),
(9, 'Birthday Party', '#FFD700', '2016-01-14 07:00:00', '2016-01-14 07:00:00'),
(10, 'Double click to change', '#008000', '2016-01-28 00:00:00', '0000-00-00 00:00:00'),
(15, 'Close for bad whether', '#FF0000', '2016-01-15 00:00:00', '2016-01-16 00:00:00'),
(16, 'Present Mockup - Daycare system', '#008000', '2019-06-21 00:00:00', '2019-06-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `children`
--

CREATE TABLE `children` (
  `child_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `last_name` varchar(120) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `birthdate` date NOT NULL,
  `blood_type` varchar(20) DEFAULT NULL,
  `alergies` varchar(300) NOT NULL,
  `image` varchar(120) DEFAULT NULL,
  `groups` enum('red','green','blue','white') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `children`
--

INSERT INTO `children` (`child_id`, `user_id`, `first_name`, `last_name`, `gender`, `birthdate`, `blood_type`, `alergies`, `image`, `groups`) VALUES
(1, 10, 'Nathan', 'Mendoza', 'male', '2017-11-15', 'A+', 'No', 'uploads/nathan_1.png', 'green'),
(5, 10, 'Christiano', 'Mendoza', 'male', '2018-08-18', 'A+', 'No alergies', 'uploads/img-20140723-wa000.jpg', 'green'),
(7, 10, 'Antonella', 'Mendoza', 'female', '2015-11-12', 'A+', 'No', 'uploads/antonella_1.png', 'blue'),
(8, 9, 'John', 'Guo', 'male', '2010-09-08', 'A-', 'No', 'uploads/jhon_1.png', 'blue'),
(9, 4, 'Mishka', 'Patel', 'male', '2015-06-15', 'A+', 'No', 'uploads/mishka.png', 'green'),
(10, 7, 'Maya', 'Mogollon', 'female', '2007-04-06', 'A+', 'eggs.', 'uploads/maya.png', 'red');

-- --------------------------------------------------------

--
-- Table structure for table `parents`
--

CREATE TABLE `parents` (
  `parent_id` int(11) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `email` varchar(120) NOT NULL,
  `homephone` varchar(50) NOT NULL,
  `cellphone` varchar(50) NOT NULL,
  `image` varchar(120) NOT NULL,
  `type_responsable` enum('father','mother','authorized_pickup') NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parents`
--

INSERT INTO `parents` (`parent_id`, `first_name`, `last_name`, `gender`, `email`, `homephone`, `cellphone`, `image`, `type_responsable`, `user_id`) VALUES
(1, 'Fiorella', 'Pujalt', 'male', 'fio@yahoo.es', '715-186-9898', '514-758-9888', 'uploads/simona.png', 'mother', 10),
(2, 'Rolly', 'Mogollon', 'male', 'rolly@yahoo.es', '580-751-9498', '514-789-8998', 'uploads/frank_10.png', 'authorized_pickup', 7),
(3, 'Techi', 'Ore', 'female', 'techi@yahoo.pe', '580-789-6495', '780-988-6158', 'uploads/user_profile_3.png', 'authorized_pickup', 10),
(5, 'Frank', 'Patel', 'male', 'frank@gmail.com', '514-780-9898', '514-780-9894', 'uploads/frank_2.png', 'father', 4),
(7, 'JONATHAN', 'MENDOZa', 'male', 'sllem1802@gmail.com', '514-715-6496', '580-809-9899', 'uploads/antonella_3.png', 'father', 9);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `email` varchar(120) NOT NULL,
  `first_name` varchar(60) NOT NULL,
  `last_name` varchar(60) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `phone_number` varchar(60) DEFAULT NULL,
  `password` varchar(120) NOT NULL,
  `image` varchar(120) DEFAULT NULL,
  `role` enum('admin','user','parent','authorized_pickup','manager','educator') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `email`, `first_name`, `last_name`, `gender`, `phone_number`, `password`, `image`, `role`) VALUES
(1, 'sllem@gmail.com', 'Sllem', 'Zapata', 'male', '514-175-6496', 'IPD2017sllem', 'uploads/sllem_1.png', 'admin'),
(3, 'antonella@gmail.com', 'Antonella', 'Vianchi', 'male', '580-645-8989', 'IPD2017sllem', 'uploads/linda_1.png', 'user'),
(4, 'rina@gmail.com', 'Rinal', 'Patel', 'male', '580-689-4598', 'IPD2017sllem', 'uploads/rina_1.png', 'parent'),
(5, 'maria@gmail.com', 'Maria', 'Melendez', 'female', '480-959-9898', 'IPD2017sllem', 'uploads/maria_1.png', 'educator'),
(7, 'rolly@hotnmail.com', 'Rolly', 'Mogollon', 'male', '780-898-6898', 'IPD2017sllem', 'uploads/rolly_1.png', 'parent'),
(8, 'simona@gmail.com', 'Simona', 'Mogollo', 'male', '580-456-9889', 'IPD2017sllem', 'uploads/simona_9.png', 'parent'),
(9, 'wu@yahoo.com', 'Wu', 'Guo', 'female', '514-454-0000', 'IPD2017sllem', 'uploads/wu_1.png', 'parent'),
(10, 'jonathan@yahoo.es', 'Jonathan', 'Mendoza', 'male', '514-715-2828', 'IPD2017sllem', 'uploads/jonathan_1.png', 'parent');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`attendance_id`),
  ADD KEY `child_id` (`child_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `staff_id` (`staff_id`) USING BTREE;

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `children`
--
ALTER TABLE `children`
  ADD PRIMARY KEY (`child_id`),
  ADD KEY `parent_id` (`user_id`);

--
-- Indexes for table `parents`
--
ALTER TABLE `parents`
  ADD PRIMARY KEY (`parent_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `attendance_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `children`
--
ALTER TABLE `children`
  MODIFY `child_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `parents`
--
ALTER TABLE `parents`
  MODIFY `parent_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendance`
--
ALTER TABLE `attendance`
  ADD CONSTRAINT `attendance_ibfk_1` FOREIGN KEY (`child_id`) REFERENCES `children` (`child_id`);

--
-- Constraints for table `children`
--
ALTER TABLE `children`
  ADD CONSTRAINT `children_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);

--
-- Constraints for table `parents`
--
ALTER TABLE `parents`
  ADD CONSTRAINT `parents_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
